#!/usr/bin/env bash
# created by: Ilay ron
# purpose: install jenkins on vm using centos
# date: 03/07/2021
# version: V1.0.3
#######################################################


yum install epel-release git java-11-openjdk-devel wget

update-alternatives --config java

sudo wget -O /etc/yum.repos.d/jenkins.repo https://pkg.jenkins.io/redhat-stable/jenkins.reposudo rpm --import https://pk

sudo rpm --import https://pkg.jenkins.io/redhat-stable/jenkins.io.key

yum install jenkins

sudo systemctl start jenkins
sudo systemctl enable jenkins
sudo systemctl status jenkins


